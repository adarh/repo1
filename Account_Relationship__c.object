<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Junction object to keep track of relationship between Parent account and child account</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>AccountContactIdnt__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>Contact ID associated with the Account</description>
        <externalId>true</externalId>
        <label>AccountContactIdnt</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Active_Relationship__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Denotes child account&apos;s relationship with parent account</description>
        <externalId>false</externalId>
        <label>Active Relationship?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Board__c</fullName>
        <description>Board name</description>
        <externalId>false</externalId>
        <label>Board</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Child_Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Child Account</description>
        <externalId>false</externalId>
        <label>Child Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Account Relationships (Child Account)</relationshipLabel>
        <relationshipName>Child_Account_Relationships</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>End_Date__c</fullName>
        <description>End Date of the relationship between parent account and child account</description>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Individual__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Look up to Contact</description>
        <externalId>false</externalId>
        <label>Individual</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Account Relationships</relationshipLabel>
        <relationshipName>Individual_Account_Relationships</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Parent_Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Parent Account</description>
        <externalId>false</externalId>
        <label>Parent Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Account Relationships</relationshipLabel>
        <relationshipName>Parent_Account_Relationships</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Release__c</fullName>
        <description>Release</description>
        <externalId>false</externalId>
        <label>Release</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Start_Date__c</fullName>
        <description>Start Date of the relationship between parent account and child account</description>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <label>Account Relationship</label>
    <nameField>
        <displayFormat>A-{0000000}</displayFormat>
        <label>Account Relationship ID</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Account Relationships</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
