<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>Board__c</fullName>
        <description>JTO 4/8/16 - Added Release 3 Boards</description>
        <externalId>false</externalId>
        <label>Board</label>
        <picklist>
            <picklistValues>
                <fullName>Medical Board</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>OTPTAT Board</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ODH Sanitarian Registration</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Cosmetology Board</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Nursing Board</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Dental Board</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Chemical Dependency Professionals Board</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>CE_Cycle__c</fullName>
        <description>Credit Requirement  Cycles</description>
        <externalId>false</externalId>
        <label>CE Cycle</label>
        <picklist>
            <picklistValues>
                <fullName>Renewal Cycle 2017</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Category__c</fullName>
        <description>Category For Credit Requirement</description>
        <externalId>false</externalId>
        <label>Category</label>
        <picklist>
            <picklistValues>
                <fullName>Cosmetology</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>End_Date__c</fullName>
        <description>Credit Requirement  for End Date</description>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Endorsement__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Credit Requirement  for endorsement</description>
        <externalId>false</externalId>
        <label>Endorsement</label>
        <referenceTo>Qualifier__c</referenceTo>
        <relationshipLabel>Credit Requirements</relationshipLabel>
        <relationshipName>Credit_Requirements</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>License_Endorsement_Type__c</fullName>
        <description>Credit Requirement  for license Endorsement</description>
        <externalId>false</externalId>
        <label>License / Endorsement Type</label>
        <picklist>
            <controllingField>Board__c</controllingField>
            <picklistValues>
                <fullName>Physical Therapist (PT)</fullName>
                <default>false</default>
                <controllingFieldValues>OTPTAT Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Acupuncturist (ACU)</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Anesthesiologist Assistant (AA)</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Approved Training Agency (ATA)</fullName>
                <default>false</default>
                <controllingFieldValues>ODH Sanitarian Registration</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Architect - Firm (ARC)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Architect - Individual (ARC)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Athletic Trainer (AT)</fullName>
                <default>false</default>
                <controllingFieldValues>OTPTAT Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Certificate of Conceded Eminence</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Chemical Dependency Counselor Assistant Preliminary</fullName>
                <default>false</default>
                <controllingFieldValues>Chemical Dependency Professionals Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Chemical Dependency Counselor Assistant</fullName>
                <default>false</default>
                <controllingFieldValues>Chemical Dependency Professionals Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Clinical Research Faculty Certificate</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Community Health Worker (CHW)</fullName>
                <default>false</default>
                <controllingFieldValues>Nursing Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Coronal Polishing</fullName>
                <default>false</default>
                <controllingFieldValues>Dental Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Cosmetic Therapist (CT)</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Cosmetologist (COS)</fullName>
                <default>false</default>
                <controllingFieldValues>Cosmetology Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Cosmetology Instructor (COSI)</fullName>
                <default>false</default>
                <controllingFieldValues>Cosmetology Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Cosmetology Salon (COSS)</fullName>
                <default>false</default>
                <controllingFieldValues>Cosmetology Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Cosmetology School (SCHL)</fullName>
                <default>false</default>
                <controllingFieldValues>Cosmetology Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Dental Hygienist</fullName>
                <default>false</default>
                <controllingFieldValues>Dental Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Dentist</fullName>
                <default>false</default>
                <controllingFieldValues>Dental Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Dialysis Technician (DT)</fullName>
                <default>false</default>
                <controllingFieldValues>Nursing Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Dialysis Technician Intern (DTI)</fullName>
                <default>false</default>
                <controllingFieldValues>Nursing Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Doctor of Osteopathic Medicine</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Doctor of Podiatric Medicine</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>DO Training Certificate</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>DPM Training Certificate</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>DPM Visiting Faculty</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Esthetician (EST)</fullName>
                <default>false</default>
                <controllingFieldValues>Cosmetology Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Esthetician Instructor (ESTI)</fullName>
                <default>false</default>
                <controllingFieldValues>Cosmetology Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Esthetics Salon (ESTS)</fullName>
                <default>false</default>
                <controllingFieldValues>Cosmetology Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Expanded Function Dental Auxiliary</fullName>
                <default>false</default>
                <controllingFieldValues>Dental Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Genetic Counselor (GC)</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Genetic Counselor (GC) - Special Activity Certificate</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Hair Designer (HD)</fullName>
                <default>false</default>
                <controllingFieldValues>Cosmetology Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Hair Design Instructor (HDI)</fullName>
                <default>false</default>
                <controllingFieldValues>Cosmetology Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Hair Design Salon (HDS)</fullName>
                <default>false</default>
                <controllingFieldValues>Cosmetology Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Independent Contractor (IC)</fullName>
                <default>false</default>
                <controllingFieldValues>Cosmetology Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Instructor (INS)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Landscape Architect - Firm (ARC)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Landscape Architect - Individual (ARC)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Licensed Chemical Dependency Counselor II</fullName>
                <default>false</default>
                <controllingFieldValues>Chemical Dependency Professionals Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Licensed Chemical Dependency Counselor III</fullName>
                <default>false</default>
                <controllingFieldValues>Chemical Dependency Professionals Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Licensed Independent Chemical Dependency Counselor</fullName>
                <default>false</default>
                <controllingFieldValues>Chemical Dependency Professionals Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Licensed Orthotist</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Licensed Pedorthist</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Licensed Practical Nurse (LPN)</fullName>
                <default>false</default>
                <controllingFieldValues>Nursing Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Licensed Practical Nurse - Temporary (IP)</fullName>
                <default>false</default>
                <controllingFieldValues>Nursing Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Licensed Prosthetist</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Licensed Prosthetist-Orthotist</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Limited Branch School</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Limited Continuing Education License</fullName>
                <default>false</default>
                <controllingFieldValues>Dental Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Limited Resident&apos;s License</fullName>
                <default>false</default>
                <controllingFieldValues>Dental Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Limited Teaching License - DDS</fullName>
                <default>false</default>
                <controllingFieldValues>Dental Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Limited Teaching License - RDH</fullName>
                <default>false</default>
                <controllingFieldValues>Dental Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Managing License (MN)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Manicuring Salon (MANS)</fullName>
                <default>false</default>
                <controllingFieldValues>Cosmetology Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Manicurist (MAN)</fullName>
                <default>false</default>
                <controllingFieldValues>Cosmetology Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Manicurist Instructor (MANI)</fullName>
                <default>false</default>
                <controllingFieldValues>Cosmetology Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Massage Therapist (MT)</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>MD Training Certificate</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Medical Doctor (MD)</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Medication Aide (MA-C)</fullName>
                <default>false</default>
                <controllingFieldValues>Nursing Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Natural Hair Styling Instructor (NHSI)</fullName>
                <default>false</default>
                <controllingFieldValues>Cosmetology Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Natural Hair Styling Salon (NHSS)</fullName>
                <default>false</default>
                <controllingFieldValues>Cosmetology Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Natural Hair Stylist (NHS)</fullName>
                <default>false</default>
                <controllingFieldValues>Cosmetology Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Occupational Therapist (OT)</fullName>
                <default>false</default>
                <controllingFieldValues>OTPTAT Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Occupational Therapy Assistant (OTA)</fullName>
                <default>false</default>
                <controllingFieldValues>OTPTAT Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Ohio Certified Prevention Specialist Assistant</fullName>
                <default>false</default>
                <controllingFieldValues>Chemical Dependency Professionals Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Ohio Certified Prevention Specialist I</fullName>
                <default>false</default>
                <controllingFieldValues>Chemical Dependency Professionals Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Ohio Certified Prevention Specialist II</fullName>
                <default>false</default>
                <controllingFieldValues>Chemical Dependency Professionals Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Oral Health Access Supervision Permit - Dentist</fullName>
                <default>false</default>
                <controllingFieldValues>Dental Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Oral Health Access Supervision Permit - Hygienist</fullName>
                <default>false</default>
                <controllingFieldValues>Dental Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Oriental Medicine (OM)</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Physical Therapy Assistant (PTA)</fullName>
                <default>false</default>
                <controllingFieldValues>OTPTAT Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Physician Assistant (PA)</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Practicing License (COS)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Radiographer</fullName>
                <default>false</default>
                <controllingFieldValues>Dental Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Radiologist Assistant (RA)</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Registered Applicant</fullName>
                <default>false</default>
                <controllingFieldValues>Chemical Dependency Professionals Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Registered Nurse (RN)</fullName>
                <default>false</default>
                <controllingFieldValues>Nursing Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Registered Nurse - Temporary (IR)</fullName>
                <default>false</default>
                <controllingFieldValues>Nursing Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Registered Sanitarian (RS)</fullName>
                <default>false</default>
                <controllingFieldValues>ODH Sanitarian Registration</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Salon (SAL)</fullName>
                <default>false</default>
                <controllingFieldValues>Cosmetology Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Sanitarian In Training (SIT)</fullName>
                <default>false</default>
                <controllingFieldValues>ODH Sanitarian Registration</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>School of Cosmetology (SCHL)</fullName>
                <default>false</default>
                <controllingFieldValues>Cosmetology Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Special Activity</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Tanning Facility (TAN)</fullName>
                <default>false</default>
                <controllingFieldValues>Cosmetology Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Telemedicine (DO)</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Telemedicine (MD)</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Temporary Dentist Volunteer Certificate</fullName>
                <default>false</default>
                <controllingFieldValues>Dental Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Test License Type</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Treatment Provider</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Visiting Clinical Professional Development Certificate</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Visiting Medical Faculty Certificate</fullName>
                <default>false</default>
                <controllingFieldValues>Medical Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Volunteer Certificate</fullName>
                <default>false</default>
                <controllingFieldValues>Dental Board</controllingFieldValues>
            </picklistValues>
            <picklistValues>
                <fullName>Volunteer Certificate (VC)</fullName>
                <default>false</default>
                <controllingFieldValues>Nursing Board</controllingFieldValues>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>License__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Credit Requirement  for license</description>
        <externalId>false</externalId>
        <label>License</label>
        <referenceTo>MUSW__License2__c</referenceTo>
        <relationshipLabel>Credit Requirements</relationshipLabel>
        <relationshipName>Credit_Requirements</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Number_of_Hours__c</fullName>
        <description>JTO 3/18 - Changed the &quot;Decimal Places&quot; from 0 to 2.</description>
        <externalId>false</externalId>
        <label>Number of Hours</label>
        <precision>7</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Start_Date__c</fullName>
        <description>Credit Requirement  for start date</description>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <label>Credit Requirement</label>
    <listViews>
        <fullName>CDP_Enforcement_Queue_Credit_Requirements</fullName>
        <filterScope>Queue</filterScope>
        <label>CDP Enforcement Queue</label>
        <queue>CDP_Enforcement_Queue</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>Cosmetology_Queue_Credit_Requirements</fullName>
        <filterScope>Queue</filterScope>
        <label>Cosmetology Queue</label>
        <queue>Cosmetology_Queue</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>NUR_License_Review_Queue_Credit_Requirements</fullName>
        <filterScope>Queue</filterScope>
        <label>NUR License Review Queue</label>
        <queue>NUR_License_Review_Queue</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>SAN_License_Review_Queue_Credit_Requirements</fullName>
        <filterScope>Queue</filterScope>
        <label>SAN License Review Queue</label>
        <queue>SAN_License_Review_Queue</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <nameField>
        <displayFormat>CREQ-{0000000}</displayFormat>
        <label>Credit Requirements Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Credit Requirements</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
